let sliders = document.querySelector("div.sliders");
let buttonState = document.querySelectorAll("button");
let screenWidth = window.screen.width;
let deplace;
screenWidth < 900 ? (deplace = 400) : (deplace = 900);

function changeState(e) {
  for (let i = 0; i < buttonState.length; i++) {
    buttonState[i].classList.replace("active", "inactive");
  }
  e.classList.replace("inactive", "active");
}

function SlideSwitchTwo(button) {
  button.addEventListener("click", (e) => {
    if (button == buttonState[0]) {
      sliders.style.left = "0px";
      changeState(button);
    } else if (button == buttonState[1]) {
      sliders.style.left = "-" + deplace + "px";
      changeState(button);
    } else if (button == buttonState[2]) {
      sliders.style.left = "-" + deplace * 2 + "px";
      changeState(button);
    } else if (button == buttonState[3]) {
      sliders.style.left = "-" + deplace * 3 + "px";
      changeState(button);
    }
  });
}

(function () {
  for (let b = 0; b < buttonState.length; b++) {
    SlideSwitchTwo(buttonState[b]);
  }
})();
